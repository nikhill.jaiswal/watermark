from PIL import Image, ImageDraw, ImageFont

def watermark_on_image(myimage):
  img = Image.open(myimage)
  width, height = img.size
  
  #resize image with 2048px keeping aspect ratio constant
  aspect_ration = width/height
  new_width = 2048
  new_height = int(new_width/aspect_ration)
  img = img.resize((new_width, new_height), Image.ANTIALIAS)
  
  #open watermark image
  watermark = Image.open('large.png')
  watermark_width, watermark_height = watermark.size
  img.paste(watermark,(0,int((new_height-watermark_height)/2)), watermark)
  img.save('test.png')
myimg = 'flag.jpg'
watermark_on_image(myimg)  